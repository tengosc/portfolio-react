import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faTwitter,
  faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons";

const HeaderNav = ({
  refAboutMe,
  refSkills,
  refPortfolio,
  refBlog,
  refContact,
}) => {
  const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop);

  return (
    <>
      <div className="toggle-menu">
        <input id="hamburger-menu" type="checkbox" />
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div className="header-container__nav">
        <div className="header-container__nav__sections">
          <ul>
            <li onClick={() => scrollToRef(refAboutMe)}>About me</li>
            <li onClick={() => scrollToRef(refSkills)}>Skills</li>
            <li onClick={() => scrollToRef(refPortfolio)}>Portfolio</li>
            <li onClick={() => scrollToRef(refBlog)}>Blog</li>
            <li onClick={() => scrollToRef(refContact)}>Contact</li>
          </ul>
        </div>

        <div className="header-container__nav__socials">
          <ul>
            <li>
              <FontAwesomeIcon className="icon" icon={faTwitter} />
            </li>
            <li>
              <FontAwesomeIcon className="icon" icon={faFacebookF} />
            </li>
            <li>
              <FontAwesomeIcon className="icon" icon={faLinkedinIn} />
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default HeaderNav;
