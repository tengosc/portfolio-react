import React from "react";
import kola_03 from "../../assets/kola_03.png";
import easyCodeButton from "../../assets/easy_code_button.png";

const AboutMe = ({ refAboutMe, refSkills }) => {
  return (
    <div className="main-about-section container">
      <div ref={refAboutMe} className="main-about-me">
        <h2>About me</h2>
        <span className="second-title">All about Technology</span>
        <div className="content-text">
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
          <p>
            Dolorum asperiores eaque unde magni minima provident enim aspernatur
            rerum ad officiis? Quas perspiciatis expedita doloremque adipisci
            nobis ipsam alias ipsum?
          </p>
          <p>
            Nulla eum ea quae harum repudiandae id cupiditate dolores tempore
            laborum ducimus.
          </p>
        </div>

        <span className="second-title">My interests</span>
        <ul className="main-about-me__interests">
          <li>music</li>
          <li>kite surfing</li>
          <li>cycling</li>
        </ul>

        <div className="main-about-me__achieve">
          <p>I completed the Easy Code course</p>
          <img src={easyCodeButton} alt="easy_code" />
        </div>
      </div>

      <div ref={refSkills} className="main-skills">
        <img className="image-circle-4" src={kola_03} alt="kolo" />
        <h2>Skills</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique,
          consequatur!
        </p>
        <div className="main-skills__plot">
          <div className="skills">
            <div className="skill-1">HTML 90%</div>
          </div>
          <div className="skills">
            <div className="skill-2">CSS 90%</div>
          </div>
          <div className="skills">
            <div className="skill-3">JS 60%</div>
          </div>
          <div className="skills">
            <div className="skill-4">Python 80%</div>
          </div>
          <div className="skills">
            <div className="skill-5">C++ 80%</div>
          </div>
          <div className="skills">
            <div className="skill-6">Git 60%</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutMe;
