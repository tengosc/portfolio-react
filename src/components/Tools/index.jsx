import React from "react";
import kola1 from "../../assets/kola_01.png";
import kola3 from "../../assets/kola_03.png";
import react from "../../assets/react.png";
import webpack from "../../assets/webpack.png";
import js from "../../assets/js.webp";
import styled from "../../assets/styled.png";
import redux from "../../assets/redux.webp";
import flexbox from "../../assets/flexbox.png";
import tool_program_ikona from "../../assets/tool_program_ikona.png";

const Tools = () => {
  return (
    <div className="main-tools">
      <img className="image-circle-5" src={kola3} alt="kolo" />
      <img className="image-circle-6" src={kola1} alt="kolo" />
      <div className="container">
        <div className="main-tools__content">
          <h2>Tools</h2>
          <span className="second-title">My essentials</span>
          <div className="main-tools_essentials">
            <div className="essential-box">
              <img src={react} alt="tool" /> React
            </div>
            <div className="essential-box">
              <img src={webpack} alt="tool" /> Webpack
            </div>
            <div className="essential-box">
              <img src={js} alt="tool" /> Express
            </div>
            <div className="essential-box">
              <img src={styled} alt="tool" /> Styled
            </div>
            <div className="essential-box">
              <img src={redux} alt="tool" /> Redux
            </div>
            <div className="essential-box">
              <img src={flexbox} alt="tool" /> Flexbox
            </div>
            <div className="essential-box">
              <img src={tool_program_ikona} alt="tool" /> Program
            </div>
            <div className="essential-box">
              <img src={tool_program_ikona} alt="tool" /> Program
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Tools;
