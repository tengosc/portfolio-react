import React from "react";
import Logo from "../../assets/logo.png";
import HeaderNav from "../HeaderNav";

const Header = ({
  refAboutMe,
  refSkills,
  refPortfolio,
  refBlog,
  refContact,
}) => {
  return (
    <div className="header">
      <div className="header-container container">
        <div className="header-container__logo">
          <img src={Logo} alt="A.A." />
        </div>
        <HeaderNav
          {...{
            refAboutMe,
            refSkills,
            refPortfolio,
            refBlog,
            refContact,
          }}
        />
      </div>
    </div>
  );
};

export default Header;
