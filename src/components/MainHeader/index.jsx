import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBitbucket, faDev } from "@fortawesome/free-brands-svg-icons";
import kola_01 from "../../assets/kola_01.png";
import kola_02 from "../../assets/kola_02.png";
import kola_03 from "../../assets/kola_03.png";
import me1 from "../../assets/me1.png";

const MainHeader = () => {
  return (
    <div className="main-header container">
      <div className="main-header__img">
        <img className="image-circle-1" src={kola_02} alt="kolo" />
        <img className="image-circle-2" src={kola_03} alt="kolo" />
        <img src={me1} alt="Me" />
      </div>

      <div className="main-header__box">
        <h1>Hi, My name is Aleksander Atamańczuk</h1>
        <span className="second-title">Software Engineer</span>
        <div className="content-text">
          <p>
            Passionate Technology and Tech Author <br />
            with 3 years of experience within the field.
          </p>
        </div>
        <div className="main-header__box__footer">
          <p>See my works</p>
          <div className="links">
            <FontAwesomeIcon className="links--icon" icon={faBitbucket} />
            <FontAwesomeIcon className="links--icon" icon={faDev} />
          </div>
        </div>
      </div>

      <div className="main-header__text">
        <img className="image-circle-3" src={kola_01} alt="kolo" />
        <h2>I am a freelancer</h2>
        <div className="content-text">
          <p>Contact me if you want to work with me</p>
        </div>
        <div className="main-header__text__buttons">
          <button type="button" className="main-btn">
            Hire me
          </button>
          <button type="button" className="main-btn">
            Download CV
          </button>
        </div>
      </div>
    </div>
  );
};

export default MainHeader;
