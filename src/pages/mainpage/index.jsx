import React, { useRef } from "react";
import Header from "../../components/Header";
import MainHeader from "../../components/MainHeader";
import AboutMe from "../../components/AboutMe";
import Tools from "../../components/Tools";

const MainPage = () => {
  const refAboutMe = useRef(null);
  const refSkills = useRef(null);
  const refPortfolio = useRef(null);
  const refBlog = useRef(null);
  const refContact = useRef(null);

  return (
    <>
      <Header
        {...{
          refAboutMe,
          refSkills,
          refPortfolio,
          refBlog,
          refContact,
        }}
      />
      <div className="main">
        <MainHeader />
        <AboutMe {...{ refAboutMe, refSkills }} />
        <Tools />
      </div>
    </>
  );
};

export default MainPage;
