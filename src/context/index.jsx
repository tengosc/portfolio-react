import React, { createContext, useRef } from "react";

export const RefContext = createContext({
  refAboutMe: null,
  refSkills: null,
  refPortfolio: null,
  refBlog: null,
  refContact: null,
});

export const RefFunction = ({ children }) => {
  const refAboutMe = useRef(null);
  const refSkills = useRef(null);
  const refPortfolio = useRef(null);
  const refBlog = useRef(null);
  const refContact = useRef(null);

  return (
    <>
      <RefContext.Provider
        value={{
          refAboutMe,
          refSkills,
          refPortfolio,
          refBlog,
          refContact,
        }}
      >
        {children}
      </RefContext.Provider>
    </>
  );
};
